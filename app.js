"use strict";

/**
 * Coin Flip API
 * flips a single coin /coinflip
 * flips a coin as many times as specified - noLimit=true for flips over 29
 * flip number must be odd
 */

const express = require("express");
const app = express();
const PORT = process.env.PORT || 5000;
const LOGGER = require("morgan");
const cors = require("cors");

app.use(LOGGER("dev"));
app.use(cors());

let healthRoute = require("./routes/healthRoute");
let coinRoute = require("./routes/coinRoute");

app.use("/health", healthRoute);
app.use("/coinflip", coinRoute);

app.listen(PORT, () =>
  console.log("Welcome to my Coin Flip API app started on port: " + PORT)
);
