"use strict";
var CoinFlip = require("./CoinFlip");

class CoinFlips {
  constructor(outcome, coinImage, outcomePercentage, individualFlips) {
    this._outcome = outcome;
    this._coinImage = coinImage;
    this._outcomePercentage = outcomePercentage;
    this._individualFlips = individualFlips;
  }

  /**
   * Passes data along correctly to parse it
   */
  toJSON() {
    return {
      outcome: this._outcome,
      coinImage: this._coinImage,
      outcomePercentage: this._outcomePercentage,
      individualFlips: this._individualFlips
    };
  }
}

module.exports = CoinFlips;
