"use strict";

class CoinFlip {
  constructor(side, coinImage) {
    this._side = side;
    this._coinImage = coinImage;
  }

  /**
   * GET /coinflip
   */
  flipCoin() {
    let randNum = this.getRandomNumber();

    if (randNum < 6) {
      this._side = "tails";
      this._coinImage =
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRF_K46xEJM8hvUAcmBh6LpbobXkpLzKthwLxTbeep6f87TmbMl";
    } else {
      this._side = "heads";
      this._coinImage =
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvGh5zRsmE29sOcMDl5JWC8S9ZfH1pnhNCxMxhrzYaIARj3u9H";
    }
  }

  getRandomNumber() {
    return Math.floor(Math.random() * 10 + 1);
  }

  setFlipNumber(flipNumber) {
    this._flipNumber = flipNumber;
  }

  /**
   * Passes data along correctly to parse it
   */
  toJSON() {
    return {
      side: this._side,
      coinImage: this._coinImage
    };
  }
}

module.exports = CoinFlip;
