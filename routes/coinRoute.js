"use strict";

const express = require("express");
const router = express.Router();
var coinController = require("../controllers/coinController");

/**
 * GET /coinflip
 * flips a coin - gives heads/tails and link to coin image
 */
router.get("/", (req, res) => {
  res.json(coinController.returnSingleCoinFlip());
});

/**
 * GET /coinflip/{integer}?noLimit=true
 * cannot have an integer passed in that is even
 * cannot have an integer passed in greater than 29 unless noLimit=true
 */
router.get("/:flips", (req, res) => {
  let flips = req.params.flips;
  let noLimit = req.query.noLimit;
  if (isNaN(flips) || (flips % 2 == 0 && flips < 30)) {
    res.status(400);
    res.json({ message: "Flips must be an odd integer." });
  }
  if (flips > 29 && !noLimit) {
    res.status(400);
    res.json({
      message: "Flips must not be more than 29 if noLimit is not set to true."
    });
  }
  if (flips > 29 && !noLimit && flips % 2 == 0) {
    res.status(400);
    res.json({ message: "Flips must not be more than 29 and must be odd." });
  }

  res.json(coinController.multipleCoinFlips(flips));
});

module.exports = router;
